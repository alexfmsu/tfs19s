package main

import (
	"fmt"
	"math"
	"runtime"
	"time"
)

func sampleNumbers() {
	fmt.Println("Numbers")
	// Объявления переменных
	var i1 int                                                  // Объявление переменной, её значение - zero value для типа
	i2 := 100                                                   // Короткая запись с инициализацией
	i3 := int64(200)                                            // Явное указание типа
	fmt.Printf("v1 = (%T)(%d), v2 = (%T)(%d), v3 = (%T)(%d)\n", // %T - формат для типа
		i1, i1, i2, i2, i3, i3,
	)

	// Floats
	f1 := 1.12          // Число с плавающей точкой. По умолчанию float64
	f2 := float32(4.56) // Но можно задать явно тип. Например, float32
	var f3 float64      // Либо объявить переменную типа float64. Её значение - zero value для типа
	// f4 := float(7.8) // Типа float в Go нет. Только float32 и float64
	fmt.Printf("f1 = (%T)(%f), f2 = (%T)(%f), f3 = (%T)(%f)\n",
		f1, f1, f2, f2, f3, f3,
	)

	// Есть комплексные числа
	c1 := complex(10, 20)
	fmt.Printf("c1 = (%T)(%v)\n", c1, c1)

	// И другие типы, объявим их блоком
	var (
		b1 byte
		b2 = byte('A') // Можно инициализировать вместе с объявлением
		r1 rune        // Алиас на int32, представляет символ в Юникоде
		r2 = rune('Ё') // Символ м.б. любым юникодом
		// b3 = byte('Ё') // Для байт такое не сработает, т.к. не каждый юникод помещается в байт
		s1 string // Строка в кодировке UTF-8
	)
	fmt.Printf("b1=%v, b2=%v r1=%v, r2=%v, s1=%v\n", b1, b2, r1, r2, s1)

	// Целочисленные типы могут быть unsigned
	ui1 := uint(math.MaxInt64) + 1
	fmt.Printf("ui1 = (%T)(%v)", ui1, ui1)
	// У каждого signed и unsigned можно указать разрядность
	// Например, int8, int16, int32, int63
}

func sampleLoops() {
	fmt.Println("Loops")
	fmt.Printf("Обычный цикл for: ")
	for i := 0; i < 2; i++ {
		fmt.Printf("%d ", i)
	}
	fmt.Println()

	var counter int
	fmt.Printf("Бесконечный цикл: ") // Выход по return либо break
	for {
		fmt.Printf("%d ", counter)
		if counter == 2 {
			break
		}
		counter++
	}

	fmt.Println("Range по слайсу: ")
	slice := []int{0, 1, 2, 3}
	for i, value := range slice {
		// value - это переменная цикла, значение которой совпадает с элементом i-ой итерции
		fmt.Printf("\tslice[%d]=%v, &value=%p, &slice[i]=%p\n", i, value, &value, &slice[i])
	}

	fmt.Printf("Range по строкe: ")
	for _, ch := range "привет,hola,嗨" {
		fmt.Printf("%c ", ch)
	}
}

func sampleSwitch() {
	fmt.Println("Switches")
	// https://tour.golang.org/flowcontrol/9
	fmt.Print("Go runs on ")
	switch os := runtime.GOOS; os {
	case "darwin":
		fmt.Println("OS X.")
	case "linux":
		fmt.Println("Linux.")
	default:
		// freebsd, openbsd,
		// plan9, windows...
		fmt.Printf("%s.", os)
	}
	fmt.Println()

	// https://tour.golang.org/flowcontrol/10
	fmt.Println("-- When's Saturday?")
	today := time.Now().Weekday()
	switch time.Saturday {
	case today + 0:
		fmt.Println("-- Today.")
	case today + 1:
		fmt.Println("-- Tomorrow.")
	case today + 2:
		fmt.Println("-- In two days.")
	default:
		fmt.Println("-- Too far away.")
	}
	fmt.Println()

	// Break писать не нужно. Если нужно "провалиться" дальше,
	// то есть ключевое слово fallthrough
}

func sampleStrings() {
	fmt.Println("Strings")
	str1 := "hello"
	fmt.Printf("str1=%s, len=%d\n", str1, len(str1))

	str2 := "привет"
	fmt.Printf("str2=%s, len=%d\n", str2, len(str2)) // len возвращает количество байт

	for i, ch := range str2 {
		fmt.Printf("\ti=%d, ch=%c\n", i, ch)
	}

	runes := []rune(str2) // строку можно привести к слайсу рун либо слайсу байт
	fmt.Printf("Runes: len=%d, runes=%+v\n", len(runes), runes)
	fmt.Println("Итерация по слайсу рун")
	for i, r := range runes {
		fmt.Printf("\ti=%d: %c, %U, %d\n", i, r, r, r)
	}
}

func sampleSlices() {
	// Слайс - структура, состоящая из 3-х машинных слов:
	//   длины (len), ёмкости (cap), и указателя на массив с данными (data)
	fmt.Println("Slices")
	var ints1 []int // nil слайс
	fmt.Printf("ints1: len=%d, cap=%d, isNil=%t, values=%+v\n",
		len(ints1), cap(ints1), ints1 == nil, ints1)

	ints2 := []int{} // пустой слайс
	fmt.Printf("ints2: len=%d, cap=%d, isNil=%t, values=%+v\n",
		len(ints2), cap(ints2), ints2 == nil, ints2)

	ints3 := []int{1, 2, 3} // Слайс с проинициализированными значениями
	fmt.Printf("ints3: len=%d, cap=%d, isNil=%t, values=%+v\n",
		len(ints3), cap(ints3), ints3 == nil, ints3)

	ints4 := make([]int, 5) // len=cap=5, все элементы - zero value для типа элементов слайса
	fmt.Printf("ints4: len=%d, cap=%d, isNil=%t, values=%+v\n",
		len(ints4), cap(ints4), ints4 == nil, ints4)

	ints5 := make([]int, 2, 5) // len=2, cap=5
	fmt.Printf("ints5: len=%d, cap=%d, isNil=%t, values=%+v\n",
		len(ints5), cap(ints5), ints5 == nil, ints5)

	// Для добавления элементов в конец есть append. Можно применить к любому слайсу, даже к nil
	ints1 = append(ints1, 1)
	fmt.Printf("Append: ints1: len=%d, cap=%d, isNil=%t, values=%+v\n",
		len(ints1), cap(ints1), ints1 == nil, ints1)

	fmt.Println("От слайсов можно взять срез:")
	slice1 := []int{1, 2, 3, 4, 5}
	slice2 := slice1[1:4]
	fmt.Printf("\tSlice 1: len=%d, cap=%d, values=%+v\n", len(slice1), cap(slice1), slice1)
	fmt.Printf("\tSlice 2: len=%d, cap=%d, values=%+v\n", len(slice2), cap(slice2), slice2)
	slice2[0] = 100
	slice2 = append(slice2, 200)
	slice1 = append(slice1, 300)
	slice2 = append(slice2, 400)
	fmt.Println("После изменений слайсов")
	fmt.Printf("\tSlice 1: len=%d, cap=%d, values=%+v\n", len(slice1), cap(slice1), slice1)
	fmt.Printf("\tSlice 2: len=%d, cap=%d, values=%+v\n", len(slice2), cap(slice2), slice2)
}

func main() {
	sampleNumbers()
	sampleLoops()
	sampleSwitch()
	sampleStrings()
	sampleSlices()
}
