package main

import (
	"fmt"
	"runtime"
	"unsafe"
)

func main() {
	// Размер типа int зависит от архитектуры процессора, для которой собралось приложение.
	// Числа с плавающей точкой по умолчанию имеют тип float64.
	// Для проверки надо собрать приложение под нужную архитектуру.
	// GOARCH=386 go run main.go
	i1 := 0
	f1 := 0.0
	fmt.Printf("GOOS=%s, GOARCH=%s\n", runtime.GOOS, runtime.GOARCH)
	fmt.Printf("\ti1 (%T) has size %d bytes\n\tf1 (%T) has size %d bytes\n",
		i1, unsafe.Sizeof(i1), f1, unsafe.Sizeof(f1),
	)

	// Примеры запуска и вывода
	//	$ GOARCH=386 go run sizes.go
	//		GOOS=darwin, GOARCH=386
	//		i1 (int) has size 4 bytes
	//		f1 (float64) has size 8 bytes
	//
	//	$ GOARCH=amd64 go run sizes.go
	//		GOOS=darwin, GOARCH=amd64
	//      i1 (int) has size 8 bytes
	//      f1 (float64) has size 8 bytes
}
