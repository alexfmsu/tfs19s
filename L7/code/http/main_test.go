package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

// Используем тестовый сервер
func TestMetricHandler_ServeHTTP_NotAllowedMethod(t *testing.T) {
	r := require.New(t)

	handler := NewMetricHandler(NewInMemoryStorage())
	mux := http.NewServeMux()
	mux.HandleFunc("/", handler.Add) // Либо без мультиплексора: http.HandleFunc("/", handler.Add)

	ts := httptest.NewServer(mux)
	client := http.Client{Timeout: time.Second}

	resp, err := client.Get(ts.URL)
	r.NoError(err)

	r.Equal(http.StatusMethodNotAllowed, resp.StatusCode)
}

// Делаем напрямую вызов тестируемой функции
func TestMetricHandler_ServeHTTP(t *testing.T) {
	r := require.New(t)

	const (
		testMetricName  = "test_metric_name"
		testMetricValue = int64(123)
	)

	storage := NewInMemoryStorage()
	metricHandler := NewMetricHandler(storage)

	handler := http.HandlerFunc(metricHandler.Add)

	requestData, err := json.Marshal(Metric{Name: testMetricName, Count: testMetricValue})
	r.NoError(err)

	req := httptest.NewRequest(http.MethodPost, "/", bytes.NewBuffer(requestData))

	recorder := httptest.NewRecorder()
	handler.ServeHTTP(recorder, req)

	respBody := recorder.Body.String()
	if recorder.Code != http.StatusOK {
		r.Equal(http.StatusOK, recorder.Code, "Wrong http code, response: %s", respBody)
	}

	r.Equal(`{"success": true}`, respBody)

	r.Equal(storage.metrics[testMetricName], testMetricValue)
}
