package primes

import (
	"math"
)

func IsPrimeNumber(number int) bool {
	if number <= 0 {
		return false
	}

	max := int(math.Sqrt(float64(number)))
	for i := 2; i < max; i++ {
		if number%i == 0 {
			return false
		}
	}

	return true
}
