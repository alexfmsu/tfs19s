package main

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	_ "github.com/lib/pq"
	"github.com/pkg/errors"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	ID        int64
	Name      string
	Email     string
	Password  string
	Birthday  *time.Time
	CreatedAt time.Time
	UpdatedAt time.Time
}

func main() {
	dsn := "postgres://user:passwd@localhost:5432/fintech" +
		"?sslmode=disable&fallback_application_name=fintech-app"
	db, err := sql.Open("postgres", dsn)
	if err != nil {
		log.Fatalf("can't connect to db: %s", err)
	}

	if err = db.Ping(); err != nil {
		log.Fatalf("can't ping db: %s", err)
	}

	passwordHash, err := bcrypt.GenerateFromPassword([]byte("vadim"), bcrypt.DefaultCost)
	if err != nil {
		log.Fatalf("can't insert generate password hash: %s", err)
	}

	fmt.Printf("%+v", string(passwordHash))

	// Вставка пользователя
	const insertUserQuery = `INSERT INTO users(name, email, password) VALUES ($1, $2, $3)`
	_, err = db.Exec(insertUserQuery, "vadim", "v.s.larionov@tinkoff.ru2", passwordHash)
	if err != nil {
		log.Fatalf("can't insert user: %s", err)
	}

	users, err := scanUsers1(db)
	if err != nil {
		log.Fatalf("can't scan users 1: %s", err)
	}

	fmt.Printf("===scanUsers1===\n")
	printUsers(users)
	fmt.Printf("--- ---\n")

	users, err = scanUsers2(db)
	if err != nil {
		log.Fatalf("can't scan users 2: %s", err)
	}
	//
	//fmt.Printf("===scanUsers2===\n")
	//printUsers(users)
	//fmt.Printf("--- ---\n")
	//
	newsStorage, err := NewUsersStorage(db)
	if err != nil {
		log.Fatalf("can't create users storage: %s", err)
	}
	defer newsStorage.Close()

	//fmt.Printf("=== FindByID ===\n")
	u, err := newsStorage.FindByID(1)
	if err != nil {
		log.Fatalf("can't find user by id: %s", err)
	}

	//fmt.Printf("User: %+v\n", *u)
}

func printUsers(users []User) {
	for _, u := range users {
		fmt.Printf("%+v\n", u)
	}
}

func scanUsers1(db *sql.DB) ([]User, error) {
	// Получить всех пользователей
	const selectUsersQuery = `SELECT id, name, email, password, birthday, created_at, updated_at FROM users ORDER BY id`
	rows, err := db.Query(selectUsersQuery)
	if err != nil {
		return nil, fmt.Errorf("can't exec query to get users: %s", err)
	}
	defer rows.Close()

	var users []User
	for rows.Next() {
		var u User
		err = rows.Scan(&u.ID, &u.Name, &u.Email, &u.Password, &u.Birthday,
			&u.CreatedAt, &u.UpdatedAt,
		)
		if err != nil {
			return nil, fmt.Errorf("can't scan row: %s", err)
		}

		users = append(users, u)
	}

	if err = rows.Err(); err != nil {
		return nil, fmt.Errorf("rows return error: %s", err)
	}

	return users, nil
}

type sqlScanner interface {
	Scan(dest ...interface{}) error
}

const userFields = `id, name, email, password, birthday, ` +
	`created_at, updated_at`

func scanUser(scanner sqlScanner, u *User) error {
	return scanner.Scan(&u.ID, &u.Name, &u.Email, &u.Password, &u.Birthday,
		&u.CreatedAt, &u.UpdatedAt,
	)
}

func scanUsers(rows *sql.Rows) ([]User, error) {
	var users []User
	var err error
	for rows.Next() {
		var u User
		if err = scanUser(rows, &u); err != nil {
			return nil, errors.Wrap(err, "can't scan user")
		}

		users = append(users, u)
	}

	if err = rows.Err(); err != nil {
		return nil, errors.Wrap(err, "rows return error")
	}

	return users, nil
}

func scanUsers2(db *sql.DB) ([]User, error) {
	// Получить всех пользователей
	const selectUsersQuery = `SELECT ` + userFields + ` FROM users ORDER BY id`
	rows, err := db.Query(selectUsersQuery)
	if err != nil {
		return nil, errors.Wrap(err, "can't exec query")
	}
	defer rows.Close()

	users, err := scanUsers(rows)
	if err != nil {
		return nil, errors.Wrap(err, "can't scan users")
	}

	return users, nil
}

type UsersStorage struct {
	statementStorage

	findByIDStmt *sql.Stmt
}

const findUserByIDQuery = `SELECT ` + userFields + ` FROM users WHERE id = $1`

func NewUsersStorage(db *sql.DB) (*UsersStorage, error) {
	storage := &UsersStorage{statementStorage: newStatementsStorage(db)}

	statements := []stmt{
		{Query: findUserByIDQuery, Dst: &storage.findByIDStmt},
	}

	if err := storage.initStatements(statements); err != nil {
		return nil, errors.Wrap(err, "can't create statements")
	}

	return storage, nil
}

func (s *UsersStorage) FindByID(id int64) (*User, error) {
	var u User
	row := s.findByIDStmt.QueryRow(id)
	if err := scanUser(row, &u); err != nil {
		return nil, errors.Wrapf(err, "can't scan user by id %d", id)
	}

	return &u, nil
}
