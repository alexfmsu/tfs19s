package main

import (
	"fmt"
	"log"
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/pkg/errors"
)

import _ "github.com/jinzhu/gorm/dialects/postgres"

type User struct {
	ID        int64 `gorm:"primary_key"`
	Name      string
	Email     string
	Password  string
	Birthday  *time.Time
	CreatedAt time.Time
	UpdatedAt time.Time
}

func (User) TableName() string {
	return "users"
}

type Lot struct {
	ID          int64 `gorm:"primary_key"`
	CreatorID   int64 `gorm:"column:creator_id"`
	Creator     *User `gorm:"ForeignKey:CreatorID;AssociationForeignKey:ID"`
	Title       string
	Description string
	MinPrice    float64
	PriceStep   float64
	CreatedAt   time.Time
	UpdatedAt   time.Time
}

func main() {
	dsn := "postgres://user:passwd@localhost:5432/fintech" +
		"?sslmode=disable&fallback_application_name=fintech-app"
	db, err := gorm.Open("postgres", dsn)
	if err != nil {
		log.Fatalf("can't connect to db: %s", err)
	}
	defer db.Close()

	db.LogMode(true)

	if err := createExample(db); err != nil {
		log.Fatalf("can't exec createExample: %s", err)
	}

	if err := selectExample(db); err != nil {
		log.Fatalf("can't exec selectExample: %s", err)
	}
}

func createExample(db *gorm.DB) error {
	u1 := User{
		Name:     "Vadim (Gorm)",
		Email:    "v.s.larionov@tinkoff.ru_gorm2",
		Password: "password_hash",
	}

	fmt.Printf("Before Create: %+v\n", u1)
	db = db.Create(&u1)
	if err := db.Error; err != nil {
		return errors.Wrap(err, "can't create user")
	}
	fmt.Printf("After create: %+v\n", u1)

	return nil
}

func selectExample(db *gorm.DB) error {
	var u User
	const email = "v.s.larionov@tinkoff.ru"
	db = db.Where(&User{Email: email}).First(&u)
	if err := db.Error; err != nil {
		return errors.Wrapf(err, "can't find user by email %q", email)
	}
	fmt.Printf("FindByEmail: %+v\n", u)

	return nil
}
