package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/go-chi/chi/middleware"

	"github.com/go-chi/chi"
)

func GetGreeting(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("trackingID=%s RealIP=%s get greeting requested\n", middleware.GetReqID(r.Context()), r.RemoteAddr)
	w.Write([]byte("Hello, anonymous!"))
}

func PostGreeting(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("trackingID=%s RealIP=%s broadcasted greeting\n", middleware.GetReqID(r.Context()), r.RemoteAddr)
	w.Write([]byte("Greetings broadcasted!"))
}

func main() {
	const MaxConcurrentRequest = 100

	r := chi.NewRouter()
	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Throttle(MaxConcurrentRequest))

	r.Route("/", func(r chi.Router) {
		r.Get("/greeting", GetGreeting)
		r.Post("/greeting", PostGreeting)
	})

	if err := http.ListenAndServe(":5000", r); err != nil {
		log.Fatal(err)
	}
}
