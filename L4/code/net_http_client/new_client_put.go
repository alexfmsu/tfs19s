package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/golang/go/src/log"
)

func main() {
	client := &http.Client{}
	putData := []byte(`{"name":"Stefa"}`)
	req, err := http.NewRequest("PUT", "https://postman-echo.com/put", bytes.NewBuffer(putData))
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Add("Content-type", "application/json")

	resp, err := client.Do(req)
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%s", body)
}
