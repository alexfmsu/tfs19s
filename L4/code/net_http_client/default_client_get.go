package main

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/golang/go/src/log"
)

func main() {
	resp, err := http.Get("https://ifconfig.co/json")
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("%s", body)
}
