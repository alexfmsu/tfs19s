package main

import (
	"log"
	"net/http"
)

func Greeting(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.NotFound(w, r)
		return
	}

	switch r.Method {
	case "GET":
		w.Write([]byte("method get implementation"))
	case "POST":
		w.Write([]byte("method post implementation"))
	default:
		w.WriteHeader(http.StatusNotImplemented)
		w.Write([]byte("method not implemented"))
	}
}

func main() {
	http.HandleFunc("/", Greeting)
	if err := http.ListenAndServe(":5000", nil); err != nil {
		log.Fatal(err)
	}
}
