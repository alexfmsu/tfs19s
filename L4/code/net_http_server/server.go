package main

import (
	"fmt"
	"log"
	"net/http"
)

func Greeting(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("%s method\n", r.Method)
	w.Write([]byte("Hello, anonymous!"))
}

func main() {
	http.HandleFunc("/", Greeting)
	if err := http.ListenAndServe(":5000", nil); err != nil {
		log.Fatal(err)
	}
}
