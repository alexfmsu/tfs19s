package main

import (
	"net/http"

	"github.com/golang/go/src/log"
)

func Greeting(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.NotFound(w, r)
		return
	}

	switch r.Header.Get("Accept") {
	case "text/plain":
		w.Header().Add("Content-type", "text/plain")
		w.Write([]byte("Hello, anonymous!"))
	case "application/json":
		w.Header().Add("Content-type", "application/json")
		w.Write([]byte("{\"greeting\":\"Hello, anonymous\"}"))
	default:
		w.Header().Add("Content-type", "text/plain")
		w.Write([]byte("Hello, anonymous!"))
	}
}

func main() {
	http.HandleFunc("/", Greeting)
	if err := http.ListenAndServe(":5000", nil); err != nil {
		log.Fatal(err)
	}
}
