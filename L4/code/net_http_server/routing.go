package main

import (
	"fmt"
	"log"
	"net/http"
	"time"
)

func Greeting(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("%s method\n", r.Method)
	w.Write([]byte("Hello, anonymous!\n"))
	time.Sleep(time.Second * 5)
	//w.Write([]byte("Another hello"))
}

func Buy(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("%s method\n", r.Method)
	w.Header().Add("X-MY-LOCATION", "ALASKA")
	w.Write([]byte("Buy, anonymous!"))
	// This header is not written, because w.Write is already occured
	w.Header().Add("X-MY-LANGUAGE", "RU")
}

func main() {
	http.HandleFunc("/", Greeting)
	http.HandleFunc("/buy/", Buy)
	if err := http.ListenAndServe(":5000", nil); err != nil {
		log.Fatal(err)
	}
}
