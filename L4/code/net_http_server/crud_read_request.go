package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func Greeting(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.NotFound(w, r)
		return
	}

	switch r.Method {
	case "GET":
		// Getting query parameters
		for k, v := range r.URL.Query() {
			fmt.Printf("%s:\t%s\n", k, v)
		}
		w.Write([]byte("method get implementation"))
	case "POST":
		// Getting body
		reqBody, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Fatal(err)
		}

		fmt.Printf("%s\n", reqBody)
		w.Write([]byte("method post implementation"))
	default:
		w.WriteHeader(http.StatusNotImplemented)
		w.Write([]byte("method not implemented"))
	}
}

func main() {
	http.HandleFunc("/", Greeting)
	if err := http.ListenAndServe(":5000", nil); err != nil {
		log.Fatal(err)
	}
}
