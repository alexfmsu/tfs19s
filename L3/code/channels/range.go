package main

import "fmt"

func main() {
	dataCh := make(chan int)
	go func() {
		defer close(dataCh)
		for i := 0; i < 5; i++ {
			dataCh <- i
		}
	}()
	for data := range dataCh {
		fmt.Printf("%v ", data)
	}
}
