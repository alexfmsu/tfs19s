package main

import (
	"fmt"
	"sync"
)

func main() {
	start := make(chan interface{})

	var wg sync.WaitGroup
	for i := 0; i < 5; i++ {
		wg.Add(1)
		go func(i int) {
			defer wg.Done()
			<-start
			fmt.Printf("%v begun\n", i)
		}(i)
	}

	fmt.Println("Unblocking...")
	close(start)
	wg.Wait()
}
