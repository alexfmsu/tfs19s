package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"tfs19s/L8/code/grpc/lots/lotspb"

	"google.golang.org/grpc"
)

func main() {
	conn, err := grpc.Dial("localhost:5000", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("can't connect to server: %v", err)
	}
	defer conn.Close()
	client := lotspb.NewLotsServiceClient(conn)

	req := lotspb.LotsRequest{
		Limit: 3,
	}

	resp, err := client.ActiveLots(context.Background(), &req)
	if err != nil {
		log.Fatalf("can't get active lots: %v", err)
	}

	for {
		lots, err := resp.Recv()
		if err != nil {
			if err == io.EOF {
				break
			}
			log.Fatalf("can't receive from server: %v", err)
		}
		fmt.Printf("active lot: %+v\n", lots.Lot)
	}
}
