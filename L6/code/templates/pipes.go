package main

import (
	"fmt"
	"log"
	"os"
	"text/template"
)

type Lot struct {
	ID         int
	Title      string
	StartPrice float64
}

const tmpl = `{{$a := 100.0}}{{eq $a .StartPrice|printf "%v"}} startPrice is 100`

func main() {
	note := Lot{1, "Часы наручные ручной работы", 100}
	t := template.New("note")
	t, err := t.Parse(tmpl)
	if err != nil {
		log.Fatal("can't parse: ", err)
		return
	}
	if err := t.Execute(os.Stdout, note); err != nil {
		log.Fatal("can't execute: ", err)
		return
	}
}
