package main

import (
	"fmt"
	"golang.org/x/net/websocket"
	"log"
	"net/http"
)

type Person struct {
	Name   string
	Emails []string
}

func ReceivePerson(ws *websocket.Conn) {
	var person Person
	err := websocket.JSON.Receive(ws, &person)
	if err != nil {
		fmt.Printf("can't receive: %s\n", err)
		return
	}
	fmt.Println("Name: " + person.Name)
	for _, email := range person.Emails {
		fmt.Printf("email %s", email)
	}
}

func main() {
	http.Handle("/", websocket.Handler(ReceivePerson))
	err := http.ListenAndServe(":5000", nil)
	if err != nil {
		log.Fatalf("can't start server: %s", err)
	}
}
