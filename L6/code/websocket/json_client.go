package main

import (
	"fmt"
	"golang.org/x/net/websocket"
)

type Person struct {
	Name   string
	Emails []string
}

func main() {
	url := "ws://localhost:5000"
	origin := "http://localhost:5000"
	conn, err := websocket.Dial(url, "", origin)

	person := Person{
		Name:   "Jan",
		Emails: []string{"ja@newmarch.name", "jan.newmarch@gmail.com"},
	}
	err = websocket.JSON.Send(conn, person)
	if err != nil {
		fmt.Printf("can't send: %s\n", err)
	}
}
