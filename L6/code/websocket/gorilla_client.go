package main

import (
	"fmt"
	"io"
	"log"
	"net/http"

	"github.com/gorilla/websocket"
)

func main() {
	url := "ws://localhost:5000"
	header := make(http.Header)
	header.Add("Origin", "http://localhost:5000")
	conn, _, err := websocket.DefaultDialer.Dial(url, header)
	if err != nil {
		log.Fatalf("can't connect: %s", err)
	}

	for {
		_, reply, err := conn.ReadMessage()
		if err != nil {
			if err == io.EOF {
				// graceful shutdown by server
				fmt.Println("EOF from server")
				break
			}
			if websocket.IsCloseError(err, websocket.CloseAbnormalClosure) {
				fmt.Println("Close from server")
				break
			}
			fmt.Println("can't receive: %s", err)
			break
		}
		fmt.Printf("received from server: %s\n", string(reply[:]))

		err = conn.WriteMessage(websocket.TextMessage, reply)
		if err != nil {
			fmt.Println("can't return msg: %s\n", err)
			break
		}
	}
}
