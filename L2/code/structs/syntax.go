package main

import (
	"fmt"
	"strings"
	"time"
)

type User struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	email     string
	Company   string `json:"company"`
}

// Допускается перечислять несколько полей одного типа на одной строке,
// но не такой вариант читается хуже
type News struct {
	Title, Body string
	PublishedAt time.Time
	Author      string
	Name        string
}

// В Go нет конструкторов, но есть функции, которыми можно создавать объекты.
// Перед созданием объект можно валидировать
func NewUser(firstName, lastName, email, company string) (*User, error) {
	if err := validateEmail(email); err != nil {
		return nil, fmt.Errorf("can't validarte email %q: %s", email, err)
	}

	user := User{
		FirstName: firstName,
		LastName:  lastName,
		email:     email,
		Company:   company,
	}

	return &user, nil
}

// Объект по умолчанию
func NewAnonymousUser() User {
	return User{
		FirstName: "User",
		email:     "anonym@tinkoff.ru",
	}
}

// Метод на объекте, не изменяет состояние объекта
func (u User) IsStaff() bool {
	return u.Company == "Tinkoff"
}

// Метод на указателе на объект, изменяет состояние объекта
func (u *User) ChangeEmail(email string) error {
	if err := validateEmail(email); err != nil {
		return fmt.Errorf("can't validate email: %s", err)
	}

	u.email = email
	return nil
}

// Функция изменяем состояние объекта
func ChangeUserEmail(user *User, email string) error {
	if err := validateEmail(email); err != nil {
		return fmt.Errorf("can't validate email: %s", err)
	}

	user.email = email
	return nil
}

func validateEmail(email string) error {
	if !strings.Contains(email, "@") {
		return fmt.Errorf("bad email format")
	}

	return nil
}

// Встраивание структур
// Теперь все методы
type Admin struct {
	User
	permissions map[string]bool
}

func NewAdmin(user User) Admin {
	return Admin{
		User:        user,                  // Можно здесь перечислить все поля пользователя, если на входе нет объекта User
		permissions: make(map[string]bool), // Инициализация map-ы
	}
}

func (a Admin) CanBlockUser() bool {
	return a.permissions["block_user"]
}

func main() {
}
