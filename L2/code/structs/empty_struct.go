package main

import (
	"fmt"
	"unsafe"
)

type Empty struct{}

func (e Empty) PrintHello() {
	fmt.Println("Hello")
}

type StructWithFields struct {
	Field1 struct{}
	Field2 [2]struct{}
}

func setExample() {
	// Строим множество
	usersSet := make(map[string]struct{})
	users := []string{"John", "Bob", "William", "Bob"}
	for _, user := range users {
		usersSet[user] = struct{}{}
	}

	// Пустую структуру удобно использовать, когда нужны только ключи и нет проверки наличия значения
	for user := range usersSet {
		fmt.Println(user)
	}
}

func setWithValuesExample() {
	admins := make(map[string]bool)
	adminUsers := []string{"Mary", "Bill", "John"}
	// Строим множество
	for _, user := range adminUsers {
		admins[user] = true
	}

	allUsers := []string{"Mary", "Kay", "Will"}
	for _, user := range allUsers {
		if admins[user] {
			fmt.Printf("%s is admin\n", user)
		}
	}
}

func main() {
	e := Empty{}
	e.PrintHello()

	fmt.Println(unsafe.Sizeof(e))

	// Проверить размеры можно через unsafe.Sizeof()

	s := StructWithFields{}
	fmt.Println(unsafe.Sizeof(s))

	fmt.Println()
	fmt.Println("setExample")
	setExample()

	fmt.Println()
	fmt.Println("setWithValuesExample")
	setWithValuesExample()
}
