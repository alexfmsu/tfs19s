package main

import "fmt"

func main() {
	var fruits map[string]int
	fmt.Printf("1: fruits: isNil=%t, len=%d\n", fruits == nil, len(fruits))

	//fruits["apple"] = 100 // Добавлять элементы нельзя, т.к. мапа не инициализирована
	fruits = make(map[string]int) // Инициализация мапы
	fmt.Printf("2: fruits: isNil=%t, len=%d\n", fruits == nil, len(fruits))
	fruits = make(map[string]int, 10) // Инициализация с заданием внутреннего размера (не путать с len)
	fmt.Printf("3: fruits: isNil=%t, len=%d\n", fruits == nil, len(fruits))

	// Либо можно создавать с иниализацией сразу
	var m1 = map[string]int{} //
	var m2 = map[string]int{
		"foo": 10,
		"bar": 20,
	}
	fmt.Printf("m1=%+v, m2=%+v\n", m1, m2)

	// Добавление элемента
	fmt.Printf("fruits: len=%d, %+v\n", len(fruits), fruits)

	fruits["avocado"] = 0
	badFruit := fruits["bad_fruit_name"] // Вернётся zero value для типа значения
	avocado := fruits["avocado"]         // Вернётся 0, т.к. выше мы явно добавили его
	fmt.Printf("avocado = %d, bad fruit = %d\n", avocado, badFruit)

	// Чтобы отличить отсутствие элемента от его нулевого значение, есть второе возвращаемое значение
	lemons, ok := fruits["lemon"]
	if !ok {
		fmt.Println("I don't know what is lemon")
	} else {
		fmt.Printf("We have %d lemons\n", lemons)
	}

	fruits["bananas"] = 30
	fmt.Printf("fruits: %+v\n", fruits)
	// Для удаления элементов по ключу есть функция delete
	delete(fruits, "apple")
	fmt.Printf("fruits after delete: %+v\n", fruits)

	fmt.Println("Циклы")
	// Порядок следования элементов в цикле не определён. Он не совпадает с порядком добавления
	for key, value := range fruits {
		fmt.Printf("\tkey=%q, value=%d\n", key, value)
	}

	// Мапы из {type}->bool удобно использовать как множества.
	// При отсутствии элемента получим false в виде zero value.
	// Например, пусть у нас есть слайс фруктов. Нужно напечатать уникальные названия фруктов.
	allFruits := []string{"apple", "lemon", "apple", "banana", "avocado", "avocado", "apple"}
	showed := make(map[string]bool)
	fmt.Println("Show fruits")
	for _, fruit := range allFruits {
		if !showed[fruit] { // Проверять наличие элемента не требуется, т.к. нас утраивает zero value
			fmt.Println("\t" + fruit)
			showed[fruit] = true
		}
	}
}
