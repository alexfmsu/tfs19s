package main

import "fmt"

func main() {
	countError := 5

	IncrementErrors(countError, 10)
	fmt.Printf("After IncrementErrors: %d\n", countError)

	Increment(&countError, 20)
	fmt.Printf("After Increment: %d\n", countError)

	countError = Add(countError, 30)
	fmt.Printf("After Add: %d\n", countError)
}

// Передача аргумента по значению
func IncrementErrors(value, incValue int) {
	value += incValue
	const threshold = 100
	if value > threshold {
		// Отправка уведомления о большом количестве ошибок
	}
}

// Передача аргумента по указателю
func Increment(ptr *int, incValue int) {
	*ptr += incValue
}

// Изменение переменной и возврат её нового значения
func Add(value, incValue int) int {
	value += incValue
	return value
}
