package main

import (
	"fmt"
)

func panicExample() {
	fmt.Println("Hello world!")

	defer func() {
		if r := recover(); r != nil {
			fmt.Printf("Ops, panic: (%T)%+v\n", r, r)
		}
	}()

	panic("Ta-da")

	fmt.Println("Zombie apocalypse has begun!")
}

func main() {
	panicExample()
	fmt.Println("Success exit")
}
