package main

import "fmt"
import _ "github.com/lib/pq"

func init() {
	Name = "Unknown"
}

var Name = myName()

func myName() string {
	return "John"
}

func main() {
	fmt.Printf("My name is %s\n", Name)
}
