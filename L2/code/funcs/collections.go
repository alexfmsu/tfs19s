package main

import "fmt"

func exampleArrays() {
	array1 := [3]int{1, 2, 3}
	fmt.Printf("Array1: %+v, len=%d, cap=%d\n", array1, len(array1), cap(array1))

	array2 := [...]int{5, 6, 7, 8}
	fmt.Printf("Array2: %+v, len=%d, cap=%d\n", array2, len(array2), cap(array2))
}

func simpleExampleSlices() {
	var slice []int
	for i := 0; i < 10; i++ {
		slice = append(slice, i)
	}
	fmt.Printf("Slice: %+v, len=%d, cap=%d\n", slice, len(slice), cap(slice))
}

func simpleExampleMaps() {
	fruits := make(map[string]int)
	fruits["Apple"] = 10
	fruits["Orange"] = 15
	fruits["Banana"] = 0

	fmt.Println("Fruits")
	for key, value := range fruits {
		fmt.Printf("%s: %d\n", key, value)
	}

	countApricot := fruits["Apricot"]
	fmt.Printf("Count apricot: %d\n", countApricot)

	if count, ok := fruits["Apricot"]; ok {
		fmt.Printf("We have apricots: %d\n", count)
	} else {
		fmt.Println("We don't have apricots")
	}

	// Удаление элементов
	delete(fruits, "Apple")
	fmt.Printf("Fruits: %+v\n", fruits)
}

func main() {
	exampleArrays()
	simpleExampleSlices()
	simpleExampleMaps()
}
