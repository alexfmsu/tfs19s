# Настройка Gitlab CI

Репозиторий с примером: https://gitlab.com/vadimlarionov/hello-app

## Закупка сервера

Для настройки Gitlab Runner-а нам понадобится виртуальный сервер на **ОС Ubuntu 18.04** (Bionic Beaver).
Я приобрёл его на https://www.scaleway.com.
Можно использовать любого другого провайдера ([DigitalOcean](https://www.digitalocean.com/), [vscale.io](https://vscale.io/), [AWS](https://aws.amazon.com/ru/) и т.п.).
Дорогой сервер покупать не нужно, 1 ядра и 1 ГБ будет достаточно. В примере я использую конфигурацию DEV1-S (2 vCPUs, 2 GB, 20 GB, €2.99/mo).

При создании виртуального сервера нужно загрузить на него публичную часть своего ssh-ключа.
Если у вас его нет, то необходимо его создать ([инструкция](https://confluence.atlassian.com/bitbucketserver/creating-ssh-keys-776639788.html)).

После того как сревер будет создан, вы сможете подключаться к нему по ssh с помощью команды
```
ssh root@{server-ip}
```

По умолчанию Scaleway не создаёт пользователей, поэтому доступен только суперпользователь (`root`).
Работать из под суперпользователя является плохой практикой (в том числе с точки зрения безопасности).
Для упрощения все команды будут приведены для пользователя `root`.

Нам понадобятся Docker, Docker Compose и Gitlab Runner.
## Docker
Оригинал: https://docs.docker.com/install/linux/docker-ce/ubuntu/

Если на сервере нет команды `sudo` (на Scaleway по умолчанию нет), то её нужно установить
```sh
apt update && apt install sudo
```

Для установки Docker выполним следующие команды:
```sh
sudo apt update && sudo apt upgrade
sudo apt install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt update
sudo apt install docker-ce docker-ce-cli containerd.io
```

Проверим, что Docker установлен:
```sh
docker --version
docker pull hello-world
docker run hello-world:latest
```

Если вы всё выполнили правильно, то увидите приветственный текст от Docker-а.

## Docker Compose
Оригинал: https://docs.docker.com/compose/install/

На момент написания статьи последняя версия docker-compose `1.24.0`.
Убедитесь, что устанавливаете последнюю версию (https://github.com/docker/compose/releases)
```sh
curl -L https://github.com/docker/compose/releases/download/1.24.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
```

Убедимся, что docker-compose установился
```sh
docker-compose --version
docker-compose --help
```

## Gitlab Runner
Оригинал: https://docs.gitlab.com/runner/install/linux-manually.html

```sh
sudo wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
sudo chmod +x /usr/local/bin/gitlab-runner
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start
```

## Регистрация Gitlab Runner-а
Оригинал: https://docs.gitlab.com/runner/register/index.html
```sh
sudo gitlab-runner register
```

В качестве URL координатора устанавливаем `https://gitlab.com`.
Регистрационный токен нужно скопировать из настроек репозитория проекта: ваш проект на Gitlab -- Settings -- CI/CD -- Runners (нажимаем Expand).
Токен распложен в разделе "Set up a specific Runner manually".

В качестве имени Runner-а вводим произвольное имя.
Теги Runner-а разделяются запятыми, могут иметь произвольные значения: `docker,tfs`.
Удобно указать тег `docker`, чтобы была возможность использовать общие Gitlab Runner-ы.
В качестве docker-executor-а вводим `docker`. Следующим шагом нужно указать docker-образ, используемый по умолчанию -- `alpine:latest`.

Разрешаем Gitlab Runner-у обращаться к Docker-у, запущенному на машине
```sh
sudo usermod -aG docker gitlab-runner
sudo -u gitlab-runner -H docker info
```

Редактируем конфигурационный файл Gitlab Runner-а с помощью редактора nano
```
nano /etc/gitlab-runner/config.toml
```
В открывшемся файле выставляем значение `privileged = true`. Сохраняем (`CTRL + O`, enter), закрываем (`CTRL + X`).

Убедимся, что значение `privileged = true` выставлено:
```sh
cat /etc/gitlab-runner/config.toml | grep "privileged"
```

Перезапустим Gitlab Runner
```sh
gitlab-runner restart
gitlab-runner status
```

Если вы всё выполнили правильно, то в настройках (ваш проект -- Settings -- CI/CD -- Runners) будет отображаться ваш Runner.
Проверим, что Gitlab Runner отображается в веб-интерфейсе.

## Настройка переменных среды Gitlab
Документация: https://docs.gitlab.com/ee/ci/variables/#via-the-ui

Чтобы выполнять ssh-команды из CI на сервера нам понадобится ssh-ключ. Его нужно сгенерировать на локальной машине (вашем компьютере).
**Запрещается в CI добавлять свой основной SSH-ключ!**.
Чтобы случайно не затереть свой основной ключ, создайте копии его приватной и публичной частей (в ОС unix файлы `~/.ssh/id_rsa` и `~/.ssh/id_rsa.pub`).
Инструкция по генерации ключа: https://docs.gitlab.com/ee/ssh/#generating-a-new-ssh-key-pair
```
ssh-keygen -o -t rsa -b 4096
```
В качестве имени указываем `id_rsa_ci`.

Поскольку Gitlab при добавлении приватной части (файл `id_rsa`) ssh-ключа в значение переменной среды Gitlab выдаёт ошибку (Invalid value), то закодируем его в base64.
**Запрещается использовать онлайн-сервисы**.
В ОС unix это выполняется так:
```sh
cat ~/.ssh/id_rsa_ci | base64
```

Копируем base64-представление приватного ssh-ключа.

В веб-интерфейсе Gitlab (ваш проект -- Settings -- CI -- Variables) добавим 3 переменные:
* DOCKER_HUB_LOGIN - Ваш логин на Docker Hub (https://hub.docker.com/)
* DOCKER_HUB_PASSWORD - Пароль на Docker Hub
* CI_SSH_PRIVATE_KEY - Приватная часть SSH-ключа для CI в base64

Сохраняем переменные. Если вы всё выполнили верно, то в веб-интерфейсе увидите:
![env variables](img/variables.png)

Чтобы наш сервер, на котором мы будем запускать приложение, разрешал доступ по приватному ключу, публичную часть необходимо добавить на него.
Для этого копируем значение **публичной** части ssh-ключа (файл `id_rsa_ci.pub` и заходим на сервер:
```
ssh root@{server-ip}
nano ~/.ssh/authorized_keys
```
В конец файла с новой строки вставляем скопированное ранее значение, сохраняем (`CTRL + O`, enter), закрываем (`CTRL + X`, enter).

Теперь можно писать `.gitlab-ci.yml` файл.
Примеры файлов `.gitlab-ci.yml`, `docker-compose.yml`, `Makefile` и `Dockerfile` есть в репозитории https://gitlab.com/vadimlarionov/hello-app
